import axios from "axios";

import ConfigData from "../../ConfigData";

const instance = axios.create({
    baseURL: ConfigData.baseApiUrl
});

instance.defaults.headers.common["UserId"] = 2;

instance.interceptors.request.use(
    config => {
        config.headers["SecondUserId"] = 3;
        return config;
    },
    error => {
        return Promise.reject(error);
    });

export default instance;