import Api from "./AxiosApi";

export class StreamPostingServiceApi {
    getPosts(streamId: number): Promise<any[]> {
        return Api.get(`/Posts/${streamId}`).then(response => response.data);
    }

    addPost(content: string, streamId: number): Promise<any> {
        return Api.post(`/Posts`, { content, streamId }).then(response => response.data);
    }
}

const apiService = new StreamPostingServiceApi();
export default apiService;