import Api from "./AxiosApi";

export class StreamsServiceApi {
    public getAllStreams(): Promise<Array<any>> {
        return Api.get("Streams").then(response => response.data);
    }

    public getMyStreams(): Promise<Array<any>> {
        return Api.get("Streams/my").then(response => response.data);
    }

    public save(streamModel: any): Promise<any> {
        return fetch("https://localhost:44374/api/Streams", {
            method: "POST",
            headers: {
                'Content-Type':'application/json'
            },
            body: JSON.stringify(streamModel)
        }).then(response => response.json());
    }

    public saveByUser(streamModel: any): Promise<any> {
        return Api.post("Streams/byuser", streamModel).then(response => response.data);
    }

    public delete(id: number): Promise<any> {
        return fetch(`https://localhost:44374/api/Streams/${id}`, {
            method: "DELETE",
            headers: {
                'Content-Type':'application/json'
            },
            body: ""
        }).then(response => response);
    }
}

const apiService = new StreamsServiceApi();
export default apiService;