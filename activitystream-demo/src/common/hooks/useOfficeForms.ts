export enum FieldTypes {
    Text = 1,
    DropDown = 2,
    Number = 3
}

interface IFieldData {
    fieldRef: any,
    type: FieldTypes,
    id: string
}

const useOfficeForm = () => {
    const fields: IFieldData[] = [];

    const register = (element: any, type?: FieldTypes | null): void => {
        if (element) {
            fields.push({
                fieldRef: element,
                id: getElementIdByType(element, type),
                type: type ? type : FieldTypes.Text
            });
        }
    }

    const validate = () => {
        let errors: any = {};
        let values: any = {};

        fields.forEach(field => {
            if (field.type === FieldTypes.DropDown) {
                if (field.fieldRef.props.required && (!field.fieldRef.selectedOptions || field.fieldRef.selectedOptions.length === 0)) {
                    errors[field.id] = "Required";
                } else {
                    values[field.id] = field.fieldRef.selectedOptions[0];
                }
            } else if (field.type === FieldTypes.Number) {
                if (field.fieldRef.props.required && !field.fieldRef.value) {
                    errors[field.id] = "Required";
                } else if (field.fieldRef.props.min && field.fieldRef.props.max) {
                    errors[field.id] = `Value must be between ${field.fieldRef.props.min} and ${field.fieldRef.props.max}`;
                } else {
                    values[field.id] = field.fieldRef.value;
                }
            } else {
                if (!field.fieldRef.value) {
                    errors[field.id] = "Required";
                } else {
                    values[field.id] = field.fieldRef.value;
                }
            }
        });

        return {
            isFormValid: Object.keys(errors).length === 0,
            errors,
            values
        };
    }

    const getElementIdByType = (element: any, type?: FieldTypes | null) => {
        if (!type) {
            return element.props.id;
        } else if (type === FieldTypes.Number) {
            return element.props.inputProps.id;
        } else {
            return element.props.id;
        }
    }

    return {
        register,
        validate
    }
};

export default useOfficeForm;