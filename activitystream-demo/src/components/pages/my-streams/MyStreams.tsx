import React from "react";
import { IColumn, Fabric, ShimmeredDetailsList } from "office-ui-fabric-react";
import withData from "../../common/withData";
import apiService, { StreamsServiceApi } from "../../../common/api.services/StreamsServiceApi";

const MyStreams: React.FC<{data: any[], loading: boolean, service: StreamsServiceApi}> = (props) => {
    const _columns: IColumn[] = [
        { key: "id", name: "Id", fieldName: "id", minWidth: 100, maxWidth: 100 },
        { key: "name", name: "Name", fieldName: "name", minWidth: 100, maxWidth: 200 },
        { key: "status", name: "Status", fieldName: "status", minWidth: 100, maxWidth: 200 },
        { key: "actions", name: "Actions", fieldName: "", minWidth: 50, maxWidth: 100 }
    ];

    return (
        <div>
            <Fabric>
                <div>My streams: (streams created by me)</div>
                <ShimmeredDetailsList
                    columns={_columns}
                    items={props.data}
                    enableShimmer={props.loading}
                />
            </Fabric>
        </div>
    );
};

const myStreamsWithData = withData(() => {
    return apiService.getMyStreams();
}, apiService, MyStreams);

export default myStreamsWithData;