import React from "react";
import { withRouter, RouteComponentProps } from "react-router";
import { Fabric, IColumn, ShimmeredDetailsList, PrimaryButton, IconButton } from "office-ui-fabric-react";

import apiService, { StreamsServiceApi } from "../../../common/api.services/StreamsServiceApi";
import withData from "../../common/withData";
import SaveStream from "./save/SaveStream";

interface IDisplayStreamsState {
    streams: any[],
    showSaveModal: boolean
};

class StreamsPage extends React.Component<{ data: any[], loading: boolean, service: StreamsServiceApi } & RouteComponentProps<{}>, IDisplayStreamsState> {
    public state: IDisplayStreamsState = {
        streams: [],
        showSaveModal: false
    };

    private _columns: IColumn[];

    constructor(props: any) {
        super(props);

        this._columns = [
            { key: "id", name: "Id", fieldName: "id", minWidth: 100, maxWidth: 100 },
            { key: "name", name: "Name", fieldName: "name", minWidth: 100, maxWidth: 200 },
            { key: "status", name: "Status", fieldName: "status", minWidth: 100, maxWidth: 200 },
            { key: "actions", name: "Actions", fieldName: "", minWidth: 50, maxWidth: 100 }
        ];
    }

    public static getDerivedStateFromProps(props: any, state: any) {
        if (!state.streams || state.streams.length === 0) {
            return {
                streams: props.data
            };
        }

        return state;
    }

    private openAddStreamDialog = (): void => {
        this.setState({
            showSaveModal: true
        });
    }

    private handleNewStream = (streamData: any): void => {
        this.setState(prevState => ({
            showSaveModal: false,
            streams: [...prevState.streams, { id: streamData.id, name: streamData.name, status: streamData.status.text }]
        }));
    }

    private handleCancelNewStream = (): void => {
        this.setState({
            showSaveModal: false
        });
    }

    private deleteExistingStream = (id: number): void => {
        this.props.service.delete(id).then(() => {
            this.setState(prevState => ({
                streams: prevState.streams.filter(x => x.id !== id)
            }));
        });
    }

    private renderItemColumn = (item?: any, index?: number, column?: IColumn): React.ReactNode => {
        if (!column) {
            return <span></span>;
        }
        
        const columnKey = column ? column.key : "";
        
        const fieldContent = item[column.fieldName as any] as string;

        switch (columnKey) {
            case "actions": {
                return <div>
                    <IconButton iconProps={{ iconName: "Forward" }} title="Go to posting" ariaLabel="Go to posting" onClick={() => this.props.history.push(`/posting/${item.id}`)} />
                    <IconButton iconProps={{ iconName: "Delete" }} title="Delete" ariaLabel="Delete" onClick={() => {this.deleteExistingStream(item.id);}} />
                </div>;
            }

            default: return <span>{fieldContent}</span>;
        }
    }

    public render(): JSX.Element {
        const { loading } = this.props;
        const { streams, showSaveModal } = this.state;

        return (
            <div>
                <Fabric>
                    <PrimaryButton text="Add stream" onClick={this.openAddStreamDialog} />
                    <div>Streams:</div>
                    <ShimmeredDetailsList
                        items={streams}
                        columns={this._columns}
                        enableShimmer={loading}
                        onRenderItemColumn={this.renderItemColumn}
                    />
                </Fabric>
                <SaveStream visible={showSaveModal} saveCallback={this.handleNewStream} cancelCallback={this.handleCancelNewStream} />
            </div>
        );
    }
}

const streamsWithData = withData(() => {
    return apiService.getAllStreams();
}, apiService, StreamsPage);

const streamsWithRouter = withRouter(streamsWithData);

export default streamsWithRouter;