import React, { useState } from "react";

import apiService, { StreamsServiceApi } from "../../../../common/api.services/StreamsServiceApi";
import { Dialog, DialogFooter } from "office-ui-fabric-react/lib/Dialog";
import { DefaultButton, PrimaryButton, TextField, Dropdown, IDropdownOption } from "office-ui-fabric-react";
import useOfficeForm, { FieldTypes } from "../../../../common/hooks/useOfficeForms";
import withService from "../../../common/withService";

const SaveStream: React.FC<{ visible: boolean, saveCallback: any, cancelCallback: any, service: StreamsServiceApi }> = (props) => {
    const statusOptions: IDropdownOption[] = [
        { key: "unpublished", text: "Unpublished" },
        { key: "live", text: "Live" },
        { key: "closed", text: "Closed" }
    ];

    const [errors, setErrors] = useState({
        name: "",
        status: ""
    });
    const { register, validate } = useOfficeForm();

    const onSave = () => {
        const { isFormValid, errors, values } = validate();
        if (!isFormValid) {
            setErrors(errors);

            return;
        }

        props.service.saveByUser({ name: values.name, status: values.status.text })
            .then((id: number) => {
                props.saveCallback({ id, ...values });
            });
    };

    return (
        <Dialog hidden={!props.visible}>
            <TextField
                label="Name"
                id="name"
                required
                componentRef={(input) => { register(input); }}
                errorMessage={errors.name}
            />
            <Dropdown placeholder="Select an status"
                label="Select a status"
                id="status"
                options={statusOptions}
                componentRef={(input) => { register(input, FieldTypes.DropDown) }}
                required
                errorMessage={errors.status} />
            <DialogFooter>
                <PrimaryButton text="Save" onClick={onSave} />
                <DefaultButton text="Cancel" onClick={props.cancelCallback} />
            </DialogFooter>
        </Dialog>
    );
}

const saveStreamWithData = withService(SaveStream, apiService);
export default saveStreamWithData;