import React, { useState } from "react";
import { Stack, ActionButton, TextField, IconButton } from "office-ui-fabric-react";

import "./PostItem.scss";

const PostItem: React.FC<{post: any}> = (props) => {

    const [postLike, setPostLike] = useState(false);
    const [postComment, setPostComment] = useState(false);

    const handlePostLike = () => {
        setPostLike(!postLike);
    }

    const handlePostComment = () => {
        setPostComment(!postComment);
    }

    return (
        <div className="post">
            <div className="post-content">
                {props.post.content}
            </div>
            <Stack horizontal horizontalAlign={"end"}>
                <ActionButton iconProps={{ iconName: postLike ? "LikeSolid" : "Like" }} onClick={handlePostLike}>
                    Like
                </ActionButton>
                <ActionButton iconProps={{ iconName: postComment ? "CommentSolid" : "Comment" }} onClick={handlePostComment}>
                    Comment
                </ActionButton>
            </Stack>
            <div className={`${postComment ? "displayed-comment-area" : "hidden-comment-area"} comment-area`}>
                <Stack styles={{ root: { width: "100%" } }}>
                    <TextField 
                        id="content"
                        styles={{ root: { width: "100%" } }} 
                        multiline 
                        rows={3} 
                        resizable={false}
                    />
                </Stack>
                <IconButton iconProps={{ iconName: "Send" }} title="Comment" ariaLabel="Comment" />
            </div>
        </div>
    );
};

export default PostItem;