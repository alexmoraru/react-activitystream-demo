import React, { useEffect, useState } from "react";
import { withRouter, RouteComponentProps } from "react-router";
import withData from "../../common/withData";
import apiService, { StreamPostingServiceApi } from "../../../common/api.services/StreamPostingServiceApi";
import { TextField, IconButton, Stack } from "office-ui-fabric-react";

import "./StreamPosting.scss";
import useOfficeForm from "../../../common/hooks/useOfficeForms";
import PostItem from "./post-item/PostItem";

const StreamPostingPage: React.FC<{ data: any[], loading: boolean, service: StreamPostingServiceApi } & RouteComponentProps<{ id: string }>> = (props) => {

    const initialPosts: any[] = [];

    const [posts, setPosts] = useState(initialPosts);
    const [postingContent, setPostingContent] = useState("");
    const { register, validate } = useOfficeForm();

    useEffect(() => {
        setPosts(props.data);
    }, [props.data]);

    const addNewPost = (): void => {
        const { isFormValid, values } = validate();

        if (!isFormValid) {
            return;
        }

        props.service.addPost(values.content, parseInt(props.match.params.id))
        .then((postId: number) => {
            values.id = postId;

            setPostingContent("");
    
            setPosts(prevState => {
                return [...prevState, values]
            });
        });
    };

    return (
        <div>
            <div className="posting-area">
                <Stack styles={{ root: { width: "100%" } }}>
                    <TextField 
                        id="content"
                        value={postingContent}
                        onChange={(input, newValue) => setPostingContent(newValue ? newValue : "")}
                        styles={{ root: { width: "100%" } }} 
                        multiline 
                        rows={3} 
                        resizable={false}
                        componentRef={(input) => { register(input); }} />
                </Stack>
                <IconButton iconProps={{ iconName: "Send" }} title="Post" ariaLabel="Post" onClick={addNewPost} />
            </div>
            <div className="posts-area">
                {
                    posts && posts.map(post => {
                        return (
                            <PostItem post={post} key={post.id} />
                        );
                    })
                }
            </div>
        </div>
    );
};

const postingWithData = withData((props: RouteComponentProps<{ id: string }>) => {
    return apiService.getPosts(parseInt(props.match.params.id));
}, apiService, StreamPostingPage);

const postingWithRouter = withRouter(postingWithData);

export default postingWithRouter;