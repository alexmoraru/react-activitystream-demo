import React from "react";

type ShadowInjected<T1, T2> = Omit<T1, keyof T2>;

export interface WithDataProps {
    data: any,
    loading: boolean,
    service: any
};

const withData = <T extends WithDataProps>(callback: Function, service: any, WrappedComponent: React.ComponentType<T>) => {
    return class extends React.Component<ShadowInjected<T, WithDataProps>, { data: any, loading: boolean }> {
        constructor(props: any) {
            super(props);

            this.state = {
                data: null,
                loading: true
            };
        }

        componentDidMount(): void {
            callback(this.props)
                .then((data: any) => {
                    this.setState({ data, loading: false });
                });
        }

        render(): JSX.Element {
            const { data, loading } = this.state;

            return (
                <WrappedComponent {...this.props as T} data={data} loading={loading} service={service} />
            )
        }
    };
}

export default withData;