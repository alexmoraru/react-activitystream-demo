import React, { ComponentType } from "react";
import { RouteProps, Route } from "react-router";
import UnauthorizedPage from "../../pages/unauthorized/Unauthorized";

const PrivateRoute: React.FC<{wrappedComponent: ComponentType<RouteProps>, path: string, authorizationKey: string} & RouteProps> = ({wrappedComponent, path, authorizationKey, ...rest}) => {
    const authKey = localStorage.getItem("authorization");
    
    return (
        <Route path={path} component={ authorizationKey === authKey ? wrappedComponent : UnauthorizedPage } {...rest} />
    )
};

export default PrivateRoute;