import React from "react";

type ShadowInjected<T1, T2> = Omit<T1, keyof T2>;

export interface WithServiceProps {
    service: any
}

const withService = <T extends WithServiceProps>(WrappedComponent: React.ComponentType<T>, apiService: any) => {
    return class extends React.Component<ShadowInjected<T, WithServiceProps>, {}> {
        render(): JSX.Element {
            return (
                <WrappedComponent {...this.props as T} service={apiService} />
            );
        }
    }
};

export default withService;