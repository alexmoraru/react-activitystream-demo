import React from "react";

import "./Sidenav.scss";
import { Link } from "react-router-dom";

const Sidenav: React.FC = () => {
    return (
        <div className="routes">
            <Link to="/">Home</Link>
            <Link to="/streams">Streams</Link>
            <Link to="/my-streams">My streams</Link>
        </div>
    )
};

export default Sidenav;