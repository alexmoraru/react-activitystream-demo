import React from "react";
import { Route } from "react-router-dom";
import HomePage from "../pages/home/Home";
import StreamsPage from "../pages/streams/Streams";
import MyStreamsPage from "../pages/my-streams/MyStreams";
import StreamPostingPage from "../pages/stream-posting/StreamPosting";
import PrivateRoute from "./routing/PrivateRoute";

const AppRouting: React.FC = () => {

    return (
        <div>
            <Route exact path="/" component={HomePage}></Route>
            <Route exact path="/streams" component={StreamsPage}></Route>
            <Route exact path="/my-streams" component={MyStreamsPage}></Route>
            <PrivateRoute exact path="/my-private-route" wrappedComponent={MyStreamsPage} authorizationKey="1"></PrivateRoute>
            <Route path="/posting/:id" component={StreamPostingPage}></Route>
        </div>
    )

};

export default AppRouting;