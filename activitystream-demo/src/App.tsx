import React from 'react';
import { BrowserRouter as Router } from "react-router-dom";

import './App.scss';
import Topnav from './components/common/layout/topnav/Topnav';
import Sidenav from './components/common/layout/sidenav/Sidenav';
import AppRouting from './components/common/AppRouting';

const App: React.FC = () => {
    return (
        <Router>
            <div className="app">
                <div className="sidenav-container">
                    <Sidenav />
                </div>
                <div className="app-container">
                    <div className="topnav-container">
                        <Topnav />
                    </div>
                    <div className="app-content">
                        <AppRouting />
                    </div>
                </div>
            </div>
        </Router>
    );
}

export default App;
