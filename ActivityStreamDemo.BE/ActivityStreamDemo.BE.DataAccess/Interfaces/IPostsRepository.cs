﻿using ActivityStreamDemo.BE.Data.Models.Post;
using System.Collections.Generic;

namespace ActivityStreamDemo.BE.DataAccess.Interfaces
{
    public interface IPostsRepository
    {
        List<PostModel> GetPosts(int streamId);

        long Add(PostModel model);
    }
}
