﻿using ActivityStreamDemo.BE.Data.Models.Stream;
using System.Collections.Generic;

namespace ActivityStreamDemo.BE.DataAccess.Interfaces
{
    public interface IStreamsRepository
    {
        List<StreamModel> GetAll();

        List<StreamModel> GetMy(int createdBy);

        long Add(StreamModel model);

        void Delete(StreamModel model);
    }
}
