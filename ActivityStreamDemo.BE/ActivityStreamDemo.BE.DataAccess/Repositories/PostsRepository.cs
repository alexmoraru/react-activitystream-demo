﻿using ActivityStreamDemo.BE.Data.Constants;
using ActivityStreamDemo.BE.Data.Models.Post;
using ActivityStreamDemo.BE.DataAccess.Interfaces;
using Dapper.Contrib.Extensions;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace ActivityStreamDemo.BE.DataAccess.Repositories
{
    public class PostsRepository : IPostsRepository
    {
        private readonly string connectionString;

        public PostsRepository(IOptions<AppConfigData> options)
        {
            connectionString = options.Value.ConnectionString;
        }

        public long Add(PostModel model)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                return connection.Insert(model);
            }
        }

        public List<PostModel> GetPosts(int streamId)
        {
            using(var connection = new SqlConnection(connectionString))
            {
                return connection.GetAll<PostModel>().Where(x => x.StreamId == streamId).ToList();
            }
        }
    }
}
