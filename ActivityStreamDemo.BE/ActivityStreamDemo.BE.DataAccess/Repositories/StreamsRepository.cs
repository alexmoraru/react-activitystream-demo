﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using ActivityStreamDemo.BE.Data.Constants;
using ActivityStreamDemo.BE.Data.Models.Stream;
using ActivityStreamDemo.BE.DataAccess.Interfaces;
using Dapper.Contrib.Extensions;
using Microsoft.Extensions.Options;

namespace ActivityStreamDemo.BE.DataAccess.Repositories
{
    public class StreamsRepository : IStreamsRepository
    {
        private readonly string connectionString;

        public StreamsRepository(IOptions<AppConfigData> options)
        {
            connectionString = options.Value.ConnectionString;
        }

        public long Add(StreamModel model)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                return connection.Insert(model);
            }
        }

        public List<StreamModel> GetAll()
        {
            using (var connection = new SqlConnection(connectionString))
            {
                return connection.GetAll<StreamModel>().ToList();
            }
        }

        public List<StreamModel> GetMy(int createdBy)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                return connection.GetAll<StreamModel>().Where(x => x.CreatedBy == createdBy).ToList();
            }
        }

        public void Delete(StreamModel model)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                connection.Delete(model);
            }
        }
    }
}
