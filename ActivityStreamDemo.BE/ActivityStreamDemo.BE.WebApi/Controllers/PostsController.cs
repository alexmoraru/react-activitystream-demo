﻿using ActivityStreamDemo.BE.Data.Models.Post.In;
using ActivityStreamDemo.BE.Manager.Services;
using Microsoft.AspNetCore.Mvc;
using System;

namespace ActivityStreamDemo.BE.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PostsController : ControllerBase
    {
        private readonly PostsService service;

        public PostsController(PostsService service)
        {
            this.service = service;
        }

        [HttpGet("{streamId}")]
        public IActionResult Get(int streamId)
        {
            var posts = this.service.Get(streamId);

            return Ok(posts);
        }

        [HttpPost]
        public IActionResult Post([FromBody]PostInput input)
        {
            var userId = HttpContext.Request.Headers["userid"];

            var id = service.Add(input, Convert.ToInt32(userId));

            return Ok(id);
        }
    }
}