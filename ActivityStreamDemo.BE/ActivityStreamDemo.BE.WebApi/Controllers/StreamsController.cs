﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ActivityStreamDemo.BE.Data.Models.Stream.In;
using ActivityStreamDemo.BE.Manager.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ActivityStreamDemo.BE.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StreamsController : ControllerBase
    {
        private readonly StreamsService service;

        public StreamsController(StreamsService service)
        {
            this.service = service;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var streams = service.GetAll();

            return Ok(streams);
        }

        [HttpGet("my")]
        public IActionResult GetMy()
        {
            var userId = HttpContext.Request.Headers["userid"];

            var streams = service.GetMy(Convert.ToInt32(userId));

            return Ok(streams);
        }

        [HttpPost]
        public IActionResult Post([FromBody]AddStreamInput input)
        {
            var id = service.Add(input, 1);

            return Ok(id);
        }

        [HttpPost("byUser")]
        public IActionResult PostByUser([FromBody]AddStreamInput input)
        {
            var userId = HttpContext.Request.Headers["userid"];

            var id = service.Add(input, Convert.ToInt32(userId));

            return Ok(id);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            service.Delete(id);

            return Ok();
        }
    }
}