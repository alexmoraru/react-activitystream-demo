﻿using ActivityStreamDemo.BE.Data.Models.Stream;
using ActivityStreamDemo.BE.Data.Models.Stream.In;
using ActivityStreamDemo.BE.Data.Models.Stream.Out;
using ActivityStreamDemo.BE.DataAccess.Interfaces;
using ActivityStreamDemo.BE.Manager.Mapper;
using System.Collections.Generic;

namespace ActivityStreamDemo.BE.Manager.Services
{
    public class StreamsService
    {
        private readonly IAppMapper mapper;
        private readonly IStreamsRepository repository;

        public StreamsService(IAppMapper mapper, IStreamsRepository repository)
        {
            this.mapper = mapper;
            this.repository = repository;
        }

        public List<StreamListViewOut> GetAll()
        {
            var streams = repository.GetAll();

            return mapper.Map<List<StreamListViewOut>>(streams);
        }

        public List<StreamListViewOut> GetMy(int createdBy)
        {
            var streams = repository.GetMy(createdBy);

            return mapper.Map<List<StreamListViewOut>>(streams);
        }

        public long Add(AddStreamInput input, int createdBy)
        {
            var model = mapper.Map<StreamModel>(input);

            model.CreatedBy = createdBy;

            return repository.Add(model);
        }

        public void Delete(int streamId)
        {
            var model = new StreamModel
            {
                Id = streamId
            };

            repository.Delete(model);
        }
    }
}
