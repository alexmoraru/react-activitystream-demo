﻿using ActivityStreamDemo.BE.Data.Models.Post;
using ActivityStreamDemo.BE.Data.Models.Post.In;
using ActivityStreamDemo.BE.Data.Models.Post.Out;
using ActivityStreamDemo.BE.DataAccess.Interfaces;
using ActivityStreamDemo.BE.Manager.Mapper;
using System.Collections.Generic;

namespace ActivityStreamDemo.BE.Manager.Services
{
    public class PostsService
    {
        private readonly IAppMapper mapper;
        private readonly IPostsRepository repository;

        public PostsService(IAppMapper mapper, IPostsRepository repository)
        {
            this.mapper = mapper;
            this.repository = repository;
        }

        public List<PostOut> Get(int streamId)
        {
            var posts = repository.GetPosts(streamId);

            return mapper.Map<List<PostOut>>(posts);
        }

        public long Add(PostInput input, int createdBy)
        {
            var model = mapper.Map<PostModel>(input);

            model.CreatedBy = createdBy;

            return repository.Add(model);
        }
    }
}
