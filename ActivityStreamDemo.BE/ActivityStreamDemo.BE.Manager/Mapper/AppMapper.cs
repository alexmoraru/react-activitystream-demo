﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace ActivityStreamDemo.BE.Manager.Mapper
{
    public class AppMapper : IAppMapper
    {
        private readonly IMapper mapper;

        public AppMapper()
        {
            var config = AppMapperConfig.Configure();
            mapper = config.CreateMapper();
        }

        public TDestination Map<TDestination>(object source)
        {
            return mapper.Map<TDestination>(source);
        }

        public List<TDestination> Map<TDestination, TSource>(List<TSource> source)
        {
            return mapper.Map<List<TSource>, List<TDestination>>(source);
        }

        public TDestination Map<TSource, TDestination>(TSource source, TDestination destination)
        {
            return mapper.Map<TSource, TDestination>(source, destination);
        }
    }
}
