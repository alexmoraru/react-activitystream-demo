﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ActivityStreamDemo.BE.Manager.Mapper
{
    public interface IAppMapper
    {
        TDestination Map<TDestination>(object source);

        TDestination Map<TSource, TDestination>(TSource source, TDestination destination);

        List<TDestination> Map<TDestination, TSource>(List<TSource> source);
    }
}
