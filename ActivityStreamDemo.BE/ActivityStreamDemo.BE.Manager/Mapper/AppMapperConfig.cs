﻿using ActivityStreamDemo.BE.Data.Models.Post;
using ActivityStreamDemo.BE.Data.Models.Post.In;
using ActivityStreamDemo.BE.Data.Models.Post.Out;
using ActivityStreamDemo.BE.Data.Models.Stream;
using ActivityStreamDemo.BE.Data.Models.Stream.In;
using ActivityStreamDemo.BE.Data.Models.Stream.Out;
using AutoMapper;

namespace ActivityStreamDemo.BE.Manager.Mapper
{
    public static class AppMapperConfig
    {
        public static MapperConfiguration Configure()
        {
            return new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<AppMapperConfigProfile>();
            });
        }
    }

    public class AppMapperConfigProfile : Profile
    {
        public AppMapperConfigProfile()
        {
            CreateMap<StreamModel, StreamListViewOut>();
            CreateMap<AddStreamInput, StreamModel>();

            CreateMap<PostModel, PostOut>();
            CreateMap<PostInput, PostModel>();
        }
    }
}
