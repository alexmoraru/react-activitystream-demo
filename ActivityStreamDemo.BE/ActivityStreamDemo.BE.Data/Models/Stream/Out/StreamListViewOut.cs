﻿namespace ActivityStreamDemo.BE.Data.Models.Stream.Out
{
    public class StreamListViewOut
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Status { get; set; }
    }
}
