﻿using Dapper.Contrib.Extensions;

namespace ActivityStreamDemo.BE.Data.Models.Stream
{
    [Table("Streams")]
    public class StreamModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Status { get; set; }

        public int CreatedBy { get; set; }
    }
}
