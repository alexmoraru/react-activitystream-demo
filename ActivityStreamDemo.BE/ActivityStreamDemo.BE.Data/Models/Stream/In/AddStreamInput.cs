﻿namespace ActivityStreamDemo.BE.Data.Models.Stream.In
{
    public class AddStreamInput
    {
        public string Name { get; set; }

        public string Status { get; set; }
    }
}
