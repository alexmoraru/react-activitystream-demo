﻿namespace ActivityStreamDemo.BE.Data.Models.Post.In
{
    public class PostInput
    {
        public string Content { get; set; }

        public int StreamId { get; set; }
    }
}
