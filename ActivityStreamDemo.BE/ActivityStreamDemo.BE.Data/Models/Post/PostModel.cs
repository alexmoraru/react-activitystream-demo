﻿using Dapper.Contrib.Extensions;

namespace ActivityStreamDemo.BE.Data.Models.Post
{
    [Table("Posts")]
    public class PostModel
    {
        public int Id { get; set; }

        public string Content { get; set; }

        public int CreatedBy { get; set; }

        public int StreamId { get; set; }
    }
}
