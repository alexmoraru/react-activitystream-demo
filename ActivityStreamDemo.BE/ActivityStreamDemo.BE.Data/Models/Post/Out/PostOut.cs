﻿namespace ActivityStreamDemo.BE.Data.Models.Post.Out
{
    public class PostOut
    {
        public int Id { get; set; }

        public string Content { get; set; }

        public int CreatedBy { get; set; }
    }
}
