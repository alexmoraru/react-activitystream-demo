﻿CREATE TABLE Streams
(
	Id int IDENTITY(1,1) PRIMARY KEY,
	Name nvarchar(500) NOT NULL,
	Status nvarchar(100) NOT NULL,
	CreatedBy int NOT NULL
);

CREATE TABLE Posts
(
	Id int IDENTITY(1,1) PRIMARY KEY,
	Content nvarchar(max) NOT NULL,
	CreatedBy int NOT NULL,
	StreamId int NOT NULL FOREIGN KEY REFERENCES dbo.Streams(Id)
);